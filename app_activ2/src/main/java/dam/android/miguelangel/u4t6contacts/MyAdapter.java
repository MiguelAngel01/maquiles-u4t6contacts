package dam.android.miguelangel.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private static MyContacts myContacts;
    private static Context context;
    private static TextView tvData;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvId;
        TextView tvName;
        TextView tvPhone;
        ImageView ivPhoto;
        View view;

        public MyViewHolder(View view){

            super(view);
            this.view = view;
            this.tvId = view.findViewById(R.id.tvId);
            this.tvName = view.findViewById(R.id.tvName);
            this.tvPhone = view.findViewById(R.id.tvPhone);
            this.ivPhoto = view.findViewById(R.id.ivPhoto);

        }

        public void bind(Contact contactData) {

            tvId.setText(contactData.getId());
            tvName.setText(contactData.getName());
            tvPhone.setText(contactData.getPhone());
            if (contactData.getPhotoUri() == null) {
                ivPhoto.setImageResource(R.drawable.android);
            } else {
                ivPhoto.setImageURI(Uri.parse(contactData.getPhotoUri()));
            }
            view.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    tvData.setText(contactData.toString());
                    tvData.setVisibility(View.VISIBLE);
                }
            });

            view.setOnLongClickListener(new View.OnLongClickListener(){
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactData.getId()));
                    intent.setData(uri);
                    context.startActivity(intent);
                    tvData.setVisibility(View.INVISIBLE);
                    myContacts.refreshContact(contactData);
                    tvName.setText(contactData.getName());
                    tvPhone.setText(contactData.getPhone());
                    return true;
                }
            });

        }

    }

    MyAdapter(MyContacts myContacts, Context context, TextView tvData){

        this.myContacts = myContacts;
        this.context = context;
        this.tvData = tvData;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View tv = LayoutInflater.from(parent.getContext())
                                                .inflate(R.layout.constraint, parent, false);
        return new MyViewHolder(tv);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.bind(myContacts.getContactData(position));

    }

    @Override
    public int getItemCount() {

        return myContacts.getCount();

    }

}
