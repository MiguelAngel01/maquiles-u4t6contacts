package dam.android.miguelangel.u4t6contacts;

import android.provider.ContactsContract;

public class Contact {

    private String id;
    private String name;
    private String phone;
    private String photoUri;
    private String contactId;
    private String lookUpKey;
    private String rawContactId;
    private String type;
    private String photoThumbnailUri;

    public Contact(String id, String name, String phone, String photoUri, String contactId, String lookUpKey, String rawContactId, String type, String photoThumbnailUri){

        this.id = id;
        this.name = name;
        this.phone = phone;
        this.photoUri = photoUri;
        this.contactId = contactId;
        this.lookUpKey = lookUpKey;
        this.rawContactId = rawContactId;
        this.type = type;
        this.photoThumbnailUri = photoThumbnailUri;

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setType(String type){
        this.type = type;
    }

    @Override
    public String toString() {

        String typePhone = "";
        switch (type){
            case "1":
                typePhone = "HOME";
                break;
            case "2":
                typePhone = "MOBILE";
                break;
            case "3":
                typePhone = "WORK";
                break;
            default:
                typePhone = "OTHER";
                break;
        }

        return  name + " " + phone + "(" + typePhone + ")\n" +
                "_ID: " + id + " CONTACT_ID: " + contactId + " RAW_CONTACT_ID: " + rawContactId + "\n" +
                "LOOKUP_KEY: " + lookUpKey;
    }
}
