package dam.android.miguelangel.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.media.Image;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.CommonDataKinds.Phone;

import java.util.ArrayList;

public class MyContacts {

    private ArrayList<Contact> myDataSet;
    private Context context;

    public MyContacts(Context context){

        this.context = context;
        this.myDataSet = getContacts();

    }

    private ArrayList<Contact> getContacts(){

        ArrayList<Contact> contactList = new ArrayList<>();

        ContentResolver contentResolver = context.getContentResolver();

        String[] projection = new String[]{
                ContactsContract.Data._ID,
                Data.CONTACT_ID,
                Data.LOOKUP_KEY,
                Data.RAW_CONTACT_ID,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                Phone.TYPE,
                Phone.PHOTO_THUMBNAIL_URI,
                Phone.PHOTO_URI
        };

        String selectionFilter = ContactsContract.Data.MIMETYPE + "='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL";

        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection,
                selectionFilter,
                null,
                ContactsContract.Data.DISPLAY_NAME + " ASC");

        if (contactsCursor != null) {
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int idIndex = contactsCursor.getColumnIndexOrThrow(Data._ID);
            int contactIdIndex = contactsCursor.getColumnIndexOrThrow(Data.CONTACT_ID);
            int lookUpKeyIndex = contactsCursor.getColumnIndexOrThrow(Data.LOOKUP_KEY);
            int rawContactIdIndex = contactsCursor.getColumnIndexOrThrow(Data.RAW_CONTACT_ID);
            int typeIndex = contactsCursor.getColumnIndexOrThrow(Phone.TYPE);
            int photoThumbnailUriIndex = contactsCursor.getColumnIndexOrThrow(Phone.PHOTO_THUMBNAIL_URI);
            int photoUriIndex = contactsCursor.getColumnIndexOrThrow(Phone.PHOTO_URI);

            while (contactsCursor.moveToNext()) {

                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                String id = contactsCursor.getString(idIndex);
                String contactId = contactsCursor.getString(contactIdIndex);
                String lookUpKey = contactsCursor.getString(lookUpKeyIndex);
                String rawContactId = contactsCursor.getString(rawContactIdIndex);
                String type = contactsCursor.getString(typeIndex);
                String photoThumbnailUri = contactsCursor.getString(photoThumbnailUriIndex);
                String photoUri = contactsCursor.getString(photoUriIndex);

                contactList.add(new Contact(id, name, number, photoUri, contactId, lookUpKey, rawContactId, type, photoThumbnailUri));

            }

            contactsCursor.close();

        }

        return contactList;

    }

    public void refreshContact(Contact contactData) {

        ContentResolver contentResolver = context.getContentResolver();

        String[] projection = new String[]{
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.TYPE
        };

        String selectionFilter = ContactsContract.Data.MIMETYPE + "='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL" + " AND " +
                ContactsContract.Data._ID + " = " + contactData.getId();

        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection,
                selectionFilter,
                null,
                ContactsContract.Data.DISPLAY_NAME + " ASC");

        if (contactsCursor != null) {
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int typeIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);

            while (contactsCursor.moveToNext()) {

                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                String type = contactsCursor.getString(typeIndex);

                contactData.setPhone(number);
                contactData.setName(name);
                contactData.setType(type);

            }

            contactsCursor.close();

        }

    }

    public Contact getContactData(int position) {

        return myDataSet.get(position);

    }

    public int getCount() {

        return myDataSet.size();

    }

}
