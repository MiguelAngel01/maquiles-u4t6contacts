package dam.android.miguelangel.u4t6contacts;

public class Contact {

    private String id;
    private String name;
    private String phone;
    private String photoUri;

    public Contact(String id, String name, String phone, String photoUri){

        this.id = id;
        this.name = name;
        this.phone = phone;
        this.photoUri = photoUri;

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getPhotoUri() {
        return photoUri;
    }
}
