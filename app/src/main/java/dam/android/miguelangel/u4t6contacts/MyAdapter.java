package dam.android.miguelangel.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private MyContacts myContacts;
    private List<Contact> contacts;

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvId;
        TextView tvName;
        TextView tvPhone;
        ImageView ivPhoto;

        public MyViewHolder(View view){

            super(view);
            this.tvId = view.findViewById(R.id.tvId);
            this.tvName = view.findViewById(R.id.tvName);
            this.tvPhone = view.findViewById(R.id.tvPhone);
            this.ivPhoto = view.findViewById(R.id.ivPhoto);

        }

        public void bind(Contact contactData) {

            tvId.setText(contactData.getId());
            tvName.setText(contactData.getName());
            tvPhone.setText(contactData.getPhone());
            if (contactData.getPhotoUri() == null) {
                ivPhoto.setImageResource(R.drawable.android);
            } else {
                ivPhoto.setImageURI(Uri.parse(contactData.getPhotoUri()));
            }

        }

    }

    MyAdapter(MyContacts myContacts){

        this.myContacts = myContacts;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View tv = LayoutInflater.from(parent.getContext())
                                                .inflate(R.layout.constraint, parent, false);
        return new MyViewHolder(tv);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.bind(myContacts.getContactData(position));

    }

    @Override
    public int getItemCount() {

        return myContacts.getCount();

    }

}
